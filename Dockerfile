FROM python:3.6-slim

WORKDIR /jenkins

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

RUN ["pytest", "-v", "--junitxml=reports/result.xml"]
CMD tail -f /dev/null

