
def add(a, b):
    return a + b


def subtraction(a, b):
    return a - b


def multiply(a, b):
    return a * b


def divide1(a, b):
    try:
        return a / b
    except ZeroDivisionError:
        return 'You cant divide by zero'

def divide(a, b):
    return a / b



if __name__ == '__main__':
    print(divide(1, 0))
