from main import add, subtraction, multiply, divide
import pytest


def test_add():
    assert add(1, 2) == 3


def test_subtraction():
    assert subtraction(1, 2) == -1


def test_multiply():
    assert multiply(1, 2) == 2


def test_divide():
    assert divide(1, 2) == 0.5


def test_divide_by_zero():
    with pytest.raises(ZeroDivisionError):
        divide(1, 0)
